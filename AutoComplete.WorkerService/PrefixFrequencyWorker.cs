using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AutoCompleteService.Interfaces;
using System.Collections.Generic;
using AutoComplete.TProcessor;
using AutoComplete.AsyncTcpNet.Entities;
using Utils.Common;
using System.IO;

namespace AutoComplete.WorkerService
{
    public class PrefixFrequencyWorker : BackgroundService, IAutoCompleteService
    {
        private readonly ILogger<PrefixFrequencyWorker> _logger;
        private readonly TextProcessor _textProcessor;

        public PrefixFrequencyWorker(IServiceProvider serviceProvider, ILogger<PrefixFrequencyWorker> logger)
        {
            _logger = logger;
            _textProcessor = new TextProcessor(serviceProvider);
        }

        public async Task<Response> AutoCompleteAsync(string prefix)
        {
            var rs = new Response() { OperationResult = new OperationResult() };
            rs.OperationResult.CommandCode = CommandCode.AutoComplete;
            rs.Words = await _textProcessor.AutoCompleteAsync(prefix);
            rs.OperationResult.ResultCode = ResultCode.Success;
            return rs;
        }

        public async Task<Response> CreateAsync(string filePath)
        {
            var rs = new Response() { OperationResult = new OperationResult() };
            rs.OperationResult.CommandCode = CommandCode.Create;
            if (!File.Exists(filePath))
            {
                rs.OperationResult.ResultCode = ResultCode.Error;
                rs.Message = $"The file doesn't exist";
                return rs;
            }
            await _textProcessor.CreateAsync(filePath);
            rs.OperationResult.ResultCode = ResultCode.Success;
            return rs;
        }

        public async Task<Response> DeleteAsync()
        {
            var rs = new Response() { OperationResult = new OperationResult() };
            rs.OperationResult.CommandCode = CommandCode.Drop;
            await _textProcessor.DeleteAsync();
            rs.OperationResult.ResultCode = ResultCode.Success;
            return rs;
        }

        public async Task<Response> UpdateAsync(string filePath)
        {
            var rs = new Response() { OperationResult = new OperationResult() };
            rs.OperationResult.CommandCode = CommandCode.Update;
            if (!File.Exists(filePath))
            {
                rs.OperationResult.ResultCode = ResultCode.Error;
                rs.Message = $"The file doesn't exist";
                return rs;
            }
            await _textProcessor.UpdateAsync(filePath);
            rs.OperationResult.ResultCode = ResultCode.Success;
            return rs;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                //_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}

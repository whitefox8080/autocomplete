﻿using AutoComplete.AsyncTcpNet;
using AutoComplete.AsyncTcpNet.Entities;
using AutoCompleteService.DAL.Configuration;
using HostEnvironment.Console.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utils.Common;

namespace AutoComplete.Client
{
    class Program : ConsoleHost
    {
        private ILogger<Program> _logger;
        public override async Task Start(string[] args)
        {
            var switchMappings = new Dictionary<string, string>()
            {
                { "-p", "port" }
            };
            var config = new ConfigurationBuilder().AddCommandLine(args, switchMappings).Build();
            if (!ParseCommandArgs(config, out AsyncTcpArgs asyncTcpArgs))
                return;

            var host = CreateHostBuilder(args).Build();
            //await host.RunAsync();

            var client = new AsyncTcpClient(host.Services)
            {
                IPAddress = IPAddress.Loopback,
                Port = asyncTcpArgs.Port.Value,
                AutoReconnect = true,
                ConnectedCallback = async (serverClient, isReconnected) =>
                {
                    await serverClient.ParseInput();
                },
                ReceivedCallback = async (serverClient, count) => 
                {
                    await serverClient.ConsoleOutput(count);
                }
            };
            await client.RunAsync();
        }

        private bool ParseCommandArgs(IConfiguration config, out AsyncTcpArgs asyncTcpArgs)
        {
            asyncTcpArgs = new AsyncTcpArgs();
            if (config.AsEnumerable().Count() == 1)
            {
                var arg = config.AsEnumerable().First();
                if (arg.Key.Equals("port"))
                {
                    asyncTcpArgs.Port = Convert.ToInt32(arg.Value);
                }
            }
            else
            {
                _logger.Info(() => $"Usage: utility_name -p <port>");
                return false;
            }
            return asyncTcpArgs.Port.HasValue;
        }

        public override async Task Stop(string[] args)
        {
            //await Task.Factory.StartNew();
        }

        public static async Task Main(string[] args)
        {
            await new Program().Run(args);
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services
                        .AddLogging(builder => builder.AddConsole())
                        .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Debug);
                });
    }
}

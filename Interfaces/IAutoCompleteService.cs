﻿using AutoComplete.AsyncTcpNet.Entities;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompleteService.Interfaces
{
    public interface IAutoCompleteService
    {
        Task<Response> CreateAsync(string filePath);
        Task<Response> UpdateAsync(string filePath);
        Task<Response> DeleteAsync();
        Task<Response> AutoCompleteAsync(string prefix);
    }
}

﻿using AutoComplete.AsyncTcpNet;
using AutoComplete.WorkerService;
using AutoCompleteService.DAL.Configuration;
using AutoCompleteService.Interfaces;
using HostEnvironment.Console.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utils.Common;

namespace AutoComplete.Server
{
    class Program : ConsoleHost
    {
        private AsyncTcpListener server;
        private ILogger<Program> _logger;

        public override async Task Start(string[] args)
        {
            var switchMappings = new Dictionary<string, string>()
            {
                { "-p", "port" },
                { "-db", "database" }
            };
            var config = new ConfigurationBuilder().AddCommandLine(args, switchMappings).Build();
            if (!ParseCommandArgs(config, out AsyncTcpArgs asyncTcpArgs))
                return;

            var host  = CreateHostBuilder(asyncTcpArgs.DbFile).Build();
            _logger = host.Services.GetService<ILogger<Program>>();
            Console.WriteLine("*** Starting ... ***");

            //await host.RunAsync();

            server = new AsyncTcpListener(host.Services)
            {
                IPAddress = IPAddress.Loopback,
                Port = asyncTcpArgs.Port.Value,
                SendCallback = async (server, args) =>
                {
                    await server.TextProcessorRequest(args);
                }
            };
            await server.RunAsync();
        }

        private static string AttachDBFilename(string dbFilename)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AutoCompleteContext"].ConnectionString;

            var builder = new SqlConnectionStringBuilder(connectionString)
            {
                AttachDBFilename = dbFilename
            };
            return builder.ConnectionString;
        }

        private bool ParseCommandArgs(IConfiguration config, out AsyncTcpArgs asyncTcpArgs)
        {
            asyncTcpArgs = new AsyncTcpArgs();
            if (config.AsEnumerable().Count() == 2)
            {
                foreach(var arg in config.AsEnumerable())
                {
                    if (arg.Key.Equals("port"))
                    {
                        asyncTcpArgs.Port = Convert.ToInt32(arg.Value);
                    }
                    else if (arg.Key.Equals("database"))
                    {
                        asyncTcpArgs.DbFile = arg.Value;
                    }
                }
            }
            else
            {
                _logger.Info(() => $"Usage: utility_name -p <port> -db <path_to_mdf>");
                return false;
            }
            return true;
        }

        public override async Task Stop(string[] args)
        {
            await Task.Factory.StartNew(() => { server.Stop(true); });
        }

        public static async Task Main(string[] args)
        {
            await new Program().Run(args);
        }

        public static IHostBuilder CreateHostBuilder(string dbFilename) =>
            Host.CreateDefaultBuilder()
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    services
                        .AddLogging(builder => builder.AddConsole())
                        .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Debug)
                        .AddTransient<AutoCompleteContext, AutoCompleteContextDbAttached>()

                        .AddSingleton<AutoCompleteContext, AutoCompleteContextDbAttached>(serviceProvider =>
                        {
                            var connectionString = AttachDBFilename(dbFilename);
                            return new AutoCompleteContextDbAttached(connectionString);
                        })
                        .AddHostedService<IAutoCompleteService, PrefixFrequencyWorker>();
                });
        
    }
}

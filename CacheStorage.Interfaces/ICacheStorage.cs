﻿using System;

namespace AutoComplete.CacheStorage.Interfaces
{
    public interface ICacheStorage<K, V>
    {
        V GetOrCreate(K key, Func<V> factory);

        bool TryGet(K key, out V item);

        void Remove(K key);
    }
}
    
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoCompleteService.DAL.Configuration;
using NUnit.Framework;

namespace AutoComplete.NUnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestDb()
        {
            using (var ctx = new AutoCompleteContext())
            {
                try
                {
                    var words = ctx.Words.ToList();
                    //words.ForEach(s => Console.WriteLine($"Word: {s}\n"));
                    Console.WriteLine($"Word count: {words.Count}");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Assert.Pass();
        }
    }
}
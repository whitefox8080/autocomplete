﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utils.Common;

namespace ConsoleAppTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[5] { 2, 3, 6, 7, 9 };
            int[] b = new int[5] { 12, 3, 16, 17, 19 };
            var filteredWords = a.Intersect(b, i => i).ToList();

            var dictionary = new Dictionary<string, int>();
            var text = "'Oh, you can't help that,' said the Cat: 'we're all mad here. I'm mad. You're mad.'";

            var punctuation = text.Where(Char.IsPunctuation).Distinct().ToArray();
            var words = text.Split().Select(x => x.Trim(punctuation));

            foreach (var word in words)
            {
                if (dictionary.TryGetValue(word, out int counter))
                {
                    dictionary[word] = ++counter;
                }
                else
                    dictionary.Add(word, 1);
            }

            Console.WriteLine("Hello World!");
        }
    }
}

﻿using AutoComplete.AsyncTcpNet.Entities;
using AutoCompleteService.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Utils.Common;

namespace AutoComplete.AsyncTcpNet
{
	/// <summary>
	/// Listens asynchronously for connections from TCP network clients.
	/// </summary>
	public class AsyncTcpListener
	{
		#region Private data

		private TcpListener tcpListener;

		private NetworkStream stream;
		private volatile bool isStopped;
		private bool closeClients;
		private readonly IAutoCompleteService _autocompleteService;
		private readonly ILogger<AsyncTcpListener> _logger;

		#endregion Private data

		#region Constructors

		/// <summary>
		/// Initialises a new instance of the <see cref="AsyncTcpListener"/> class.
		/// </summary>
		public AsyncTcpListener(IServiceProvider serviceProvider)
		{
			_autocompleteService = serviceProvider.GetService<IAutoCompleteService>();
			_logger = serviceProvider.GetService<ILogger<AsyncTcpListener>>();
		}

		#endregion Constructors

		#region Events

		/// <summary>
		/// Occurs when a trace message is available.
		/// </summary>
		public event EventHandler<AsyncTcpEventArgs> Message;

		#endregion Events

		#region Properties

		/// <summary>
		/// Gets or sets the local IP address to listen on. Default is all network interfaces.
		/// </summary>
		public IPAddress IPAddress { get; set; }

		/// <summary>
		/// Gets or sets the port on which to listen for incoming connection attempts.
		/// </summary>
		public int Port { get; set; }

		/// <summary>
		/// Called when a pending connection request was accepted. When this method completes, the
		/// client connection will be closed.
		/// </summary>
		/// <remarks>
		/// This callback method may not be called when the <see cref="OnClientConnected"/> method
		/// is overridden by a derived class.
		/// </remarks>
		public Func<TcpClient, Task> ClientConnectedCallback { get; set; }
		public Func<AsyncTcpListener, TextProcessorEventArgs, Task> SendCallback { get; set; }

		#endregion Properties

		#region Public methods

		/// <summary>
		/// Starts listening asynchronously for incoming connection requests.
		/// </summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		public async Task RunAsync()
		{
			if (tcpListener != null)
				throw new InvalidOperationException("The listener is already running.");
			if (Port <= 0 || Port > ushort.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(Port));

			isStopped = false;
			closeClients = false;

			tcpListener = new TcpListener(IPAddress, Port);
			//tcpListener.Server.DualMode = false;
			tcpListener.Start();
			Console.WriteLine("*** Started ***");
			Console.WriteLine("*** Press Ctrl+C to exit ***\n");

			Message?.Invoke(this, new AsyncTcpEventArgs("Waiting for connections"));

			var clients = new ConcurrentDictionary<TcpClient, bool>();   // bool is dummy, never regarded
			var clientTasks = new List<Task>();
			try
			{
				TcpClient tcpClient = null;
				while (true)
				{
					try
					{
						if (tcpListener.Pending())
						{
							tcpClient = await tcpListener.AcceptTcpClientAsync();
							var clientTask = Task.Run(async () =>
							{
								await OnClientConnected(tcpClient);
							});
						}
					}
					catch (ObjectDisposedException) when (isStopped)
					{
						// Listener was stopped
						break;
					}

					if(tcpClient != null)
					{
						byte[] buffer = new byte[10240];
						stream = tcpClient.GetStream();
						int readLength;
						try
						{
							readLength = await stream.ReadAsync(buffer, 0, buffer.Length);
						}
						catch (IOException ex) when ((ex.InnerException as SocketException)?.ErrorCode == (int)SocketError.OperationAborted)
						{
							Message?.Invoke(this, new AsyncTcpEventArgs("Connection closed locally", ex));
							readLength = -1;
						}
						catch (IOException ex) when ((ex.InnerException as SocketException)?.ErrorCode == (int)SocketError.ConnectionAborted)
						{
							Message?.Invoke(this, new AsyncTcpEventArgs("Connection aborted", ex));
							readLength = -1;
						}
						catch (IOException ex) when ((ex.InnerException as SocketException)?.ErrorCode == (int)SocketError.ConnectionReset)
						{
							Message?.Invoke(this, new AsyncTcpEventArgs("Connection reset remotely", ex));
							readLength = -2;
						}
						var segment = new ArraySegment<byte>(buffer, 0, readLength);
						var args = ByteArraySerializer.Deserialize<TextProcessorEventArgs>(buffer);

						await OnSendAsync(args);
					}
				}
			}
			finally
			{
				if (closeClients)
				{
					Message?.Invoke(this, new AsyncTcpEventArgs("Shutting down, closing all client connections"));
					foreach (var tcpClient in clients.Keys)
					{
						tcpClient.Dispose();
					}
					await Task.WhenAll(clientTasks);
					Message?.Invoke(this, new AsyncTcpEventArgs("All client connections completed"));
				}
				else
				{
					Message?.Invoke(this, new AsyncTcpEventArgs("Shutting down, client connections remain open"));
				}
				clientTasks.Clear();
				tcpListener = null;
			}
		}

		public async Task Send(ArraySegment<byte> data)
		{
			await stream.WriteAsync(data.Array, data.Offset, data.Count);
		}

		public async Task<bool> TextProcessorRequest(TextProcessorEventArgs args)
		{
			var rs = new Response();
			try
			{
				if (args.Command.Equals(CommandCode.Create))
				{
					rs = await _autocompleteService.CreateAsync(args.FileName);
				}
				else if (args.Command.Equals(CommandCode.Update))
				{
					rs = await _autocompleteService.UpdateAsync(args.FileName);
				}
				else if (args.Command.Equals(CommandCode.Drop))
				{
					rs = await _autocompleteService.DeleteAsync();
				}
				else if (args.Command.Equals(CommandCode.AutoComplete))
				{
					rs = await _autocompleteService.AutoCompleteAsync(args.Prefix);
				}
			}
			catch (Exception e)
			{
				rs.OperationResult.ResultCode = ResultCode.Error;
				rs.Message = e.InnerException.Message;
				return false;
			}
			var ba = ByteArraySerializer.Serialize<Response>(rs);
			var data = new ArraySegment<byte>(ba);
			await Send(data);

			return true;
		}

		/// <summary>
		/// Closes the listener.
		/// </summary>
		/// <param name="closeClients">Specifies whether accepted connections should be closed, too.</param>
		public void Stop(bool closeClients)
		{
			if (tcpListener == null)
				throw new InvalidOperationException("The listener is not started.");

			this.closeClients = closeClients;
			isStopped = true;
			tcpListener.Stop();
		}

		#endregion Public methods

		#region Protected virtual methods

		/// <summary>
		/// Called when a pending connection request was accepted. When this method completes, the
		/// client connection will be closed.
		/// </summary>
		/// <param name="tcpClient">The <see cref="TcpClient"/> that represents the accepted connection.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		protected virtual Task OnClientConnected(TcpClient tcpClient)
		{
			if (ClientConnectedCallback != null)
			{
				return ClientConnectedCallback(tcpClient);
			}
			return Task.CompletedTask;
		}

		protected virtual Task OnSendAsync(TextProcessorEventArgs args)
		{
			if (SendCallback != null)
			{
				return SendCallback(this, args);
			}
			return Task.CompletedTask;
		}

		#endregion Protected virtual methods
    }

    /// <summary>
    /// Listens asynchronously for connections from TCP network clients.
    /// </summary>
    /// <typeparam name="TClient">The type to instantiate for accepted connection requests.</typeparam>
    public class AsyncTcpListener<TClient>
		: AsyncTcpListener
		where TClient : AsyncTcpClient, new()
	{
		#region Constructors

		/// <summary>
		/// Initialises a new instance of the <see cref="AsyncTcpListener{TClient}"/> class.
		/// </summary>
		public AsyncTcpListener(IServiceProvider serviceProvider): base(serviceProvider)
		{
		}

		#endregion Constructors

		#region Overridden methods

		/// <summary>
		/// Instantiates a new <see cref="AsyncTcpClient"/> instance of the type
		/// <typeparamref name="TClient"/> that runs the accepted connection.
		/// </summary>
		/// <param name="tcpClient">The <see cref="TcpClient"/> that represents the accepted connection.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <remarks>
		/// This implementation does not call the <see cref="OnClientConnected"/> callback method.
		/// </remarks>
		protected override Task OnClientConnected(TcpClient tcpClient)
		{
			var client = new TClient
			{
				ServerTcpClient = tcpClient
			};
			return client.RunAsync();
		}

		#endregion Overridden methods
	}

	public class AsyncTcpArgs
	{
		public int? Port { get; set; }
		public string DbFile { get; set; }
	}

}

﻿using System;
using CommandLine;
using AutoComplete.TProcessor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HostEnvironment.Console.Common;
using Utils.Common;
using System.Threading.Tasks;
using System.IO;
using AutoCompleteService.DAL.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace AutoComplete.Cli
{
    class Program: ConsoleHost
    {
        public class Options
        {
            [Option('c', "create", Required = false, HelpText = "Create 'Words' table")]
            public bool Create { get; set; }

            [Option('u', "update", Required = false, HelpText = "Update 'Words' table")]
            public bool Update { get; set; }

            [Option('d', "delete", Required = false, HelpText = "Delete 'Words' table")]
            public bool Delete { get; set; }
        }

        private ILogger<Program> _logger;
        public IConfiguration _configuration { get; set; }

        public override async Task Start(string[] args)
        {
            //var serviceCollection = new ServiceCollection();
            //ConfigureServices(serviceCollection);
            //IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            var host = CreateHostBuilder(args).Build();
            _logger = host.Services.GetService<ILogger<Program>>();

            var textProcessor = new TextProcessor(host.Services);
            var cki = new ConsoleKeyInfo();

            var switchMappings = new Dictionary<string, string>()
             {
                 { "-c", "create" },
                 { "-u", "update" },
                 { "-d", "delete" }
             };
            var config = new ConfigurationBuilder().AddCommandLine(args, switchMappings).Build();
            await ParseCommandArgs(config, host.Services);

            string input = "";
            
            while (cki.Key != ConsoleKey.Escape && cki.Key != ConsoleKey.Enter)
            {
                if (!cki.KeyChar.Equals('\0'))
                    input = cki.KeyChar + Console.ReadLine();
                else
                    input = Console.ReadLine();
                if (string.IsNullOrEmpty(input))
                    break;

                var list = await textProcessor.AutoCompleteAsync(input);
                list.ForEach(s => Console.WriteLine($"-- {s}"));

                cki = Console.ReadKey();
            };
            _logger.Info(() => $"*** Press Ctrl+C to shut down ***");
        }

        public override async Task Stop(string[] args)
        {
            //await Task.Factory.StartNew();
        }

        private async Task ParseCommandArgs(IConfiguration config, IServiceProvider provider)
        {
            var textProcessor = new TextProcessor(provider);
            if (config.AsEnumerable().Count() == 1)
            {
                var arg = config.AsEnumerable().First();
                if (arg.Key.Equals("create"))
                {
                    _logger.Debug(() => $"Create mode enabled. Current Argument: -c { arg.Value}");
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), arg.Value);
                    await textProcessor.CreateAsync(filePath);
                    _logger.Debug(() => $"Operation is completed");
                }
                else if (arg.Key.Equals("update"))
                {
                    _logger.Debug(() => $"Update mode enabled. Current Argument: -c { arg.Value}");
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), arg.Value);
                    await textProcessor.UpdateAsync(filePath);
                    _logger.Debug(() => $"Operation is completed");
                }
                else if (arg.Key.Equals("delete"))
                {
                    _logger.Debug(() => $"Delete mode enabled. Current Argument: -c { arg.Value}");
                    await textProcessor.DeleteAsync();
                    _logger.Debug(() => $"Operation is completed");
                }
                //_logger.Info(() => $"Usage: utility_name -c <file.txt> [-u <file.txt>][-d]");
            }
        }
        
        private void ConfigureServices(IServiceCollection services)
        {
            services
                .AddLogging(/*builder => builder.AddConsole()*/)
                .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Debug)
                .AddTransient<AutoCompleteContext>()
                .AddTransient<Program>();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services
                        .AddLogging(builder => builder.AddConsole())
                        .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Debug)
                        .AddTransient<AutoCompleteContext>()
                        .AddTransient<Program>();
                });

        public static async Task Main(string[] args)
        {
            await new  Program().Run(args);
        }
    }
}

﻿using AutoCompleteService.DAL.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AutoComplete.TProcessor
{
    interface ITextProcessor
    {
        Task CreateAsync(string filePath);
        Task UpdateAsync(string filePath);
        Task DeleteAsync();
        Task<List<string>> AutoCompleteAsync(string prefix);
    }
}

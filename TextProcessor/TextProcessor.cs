﻿using System;
using Microsoft.Extensions.Logging;
using Utils.Common;
using Microsoft.Extensions.DependencyInjection;
using AutoCompleteService.DAL.Configuration;
using AutoCompleteService.DAL.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using System.Data.SqlClient;

namespace AutoComplete.TProcessor
{
    public class TextProcessor : ITextProcessor
    {
        private readonly ILogger<TextProcessor> _logger;
        private readonly AutoCompleteContext _ctx;
        
        public TextProcessor(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<TextProcessor>>();
            _ctx = serviceProvider.GetService<AutoCompleteContext>();

            //_logger.Debug(() => $"TextProcessor is ready to start...");
        }

        public async Task<List<string>> AutoCompleteAsync(string prefix)
        {
            var filteredWords = new List<string>();
            if (_ctx == null)
            {
                _logger.Error(() => $"Repository is not initialized");
                return null;
            }
            try
            {
                var filteredDbWords = await GetPrefixList(prefix);
                
                filteredWords = filteredDbWords.OrderByDescending(_ => _.Qty)
                    .Take(5)
                    .Select(_ => _.Value)
                    .ToList();
            }
            catch (FileNotFoundException ex)
            {
                _logger.Error(() => ex.Message);
            }
            return filteredWords;
        }

        public async Task CreateAsync(string filePath)
        {
            var dbWords = new List<WordDto>();
            if (_ctx == null)
            {
                _logger.Error(() => $"Repository is not initialized");
                return;
            }
            try
            {
                dbWords = await UploadWordList(filePath);
            }
            catch (FileNotFoundException ex)
            {
                _logger.Error(() => ex.Message);
            }
            await DeleteAsync();
            
            _ctx.Words.AddRange(dbWords);
            _ctx.SaveChanges();
            var count = _ctx.Words.Count();
            _logger.Info(() => $"Inserted items - {count}");
        }

        public async Task DeleteAsync()
        {
            if(_ctx == null)
            {
                _logger.Error(() => $"Repository is not initialized");
                return;
            }
            await _ctx.Database.ExecuteSqlCommandAsync(@"
                delete from Words;");
        }

        public async Task UpdateAsync(string filePath)
        {
            var filteredWords = new List<WordDto>();
            if (_ctx == null)
            {
                _logger.Error(() => $"Repository is not initialized");
                return;
            }
            try
            {
                filteredWords = await UploadWordList(filePath);
            }
            catch (FileNotFoundException ex)
            {
                _logger.Error(() => ex.Message);
            }

            // updated words
            var filteredOldWords = filteredWords.Intersect(_ctx.Words, i => i.Value).ToList();
            foreach(var item in filteredOldWords)
            {
                var dbWord = _ctx.Words.Where(_ => _.Value.Equals(item.Value)).FirstOrDefault();
                if (dbWord != null) dbWord.Qty += item.Qty;
            }

            // new words
            var filteredNewWords = filteredWords.Except(_ctx.Words, i => i.Value).ToList();

            _ctx.Words.AddRange(filteredNewWords);
            _ctx.SaveChanges();
        }

        public async Task<List<WordDto>> GetPrefixList(string prefix)
        {
            var filteredWords = _ctx.Words.Where(_ => _.Value.StartsWith(prefix));
            return filteredWords.ToList();
        }

        public async Task<List<WordDto>> UploadWordList(string filePath)
        {
            string nextLine, curWord;
            var dictionary = new Dictionary<string, int>();
            var dbWords = new List<WordDto>();

            using StreamReader reader = new StreamReader(filePath);
            while ((nextLine = await reader.ReadLineAsync()) != null)
            {
                var punctuation = nextLine.Where(Char.IsPunctuation).Distinct().ToArray();
                var words = nextLine.Split().Select(x => x.Trim(punctuation));

                foreach (var word in words)
                {
                    curWord = word.ToLower();
                    if (curWord.Length > 15 || curWord.Length < 3) continue;
                    if (dictionary.TryGetValue(curWord, out int counter))
                        dictionary[curWord] = ++counter;
                    else
                        dictionary.Add(curWord, 1);
                }
            }
            dictionary = dictionary.Where(_ => _.Value >= 3).ToDictionary(_ => _.Key, _ => _.Value);
            foreach (var word in dictionary)
            {
                dbWords.Add(new WordDto() {  Value = word.Key, Qty = word.Value });
            }
            return dbWords;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Utils.Common
{
    public static class ByteArraySerializer
    {
        static public byte[] Serialize<T>(T obj)
        {
            if (obj == null)
                return null;
            var bf = new BinaryFormatter();
            using var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        static public T Deserialize<T>(byte[] data)
        {
            if (data == null)
                return default;
            var bf = new BinaryFormatter();
            using var ms = new MemoryStream(data);
            object obj = bf.Deserialize(ms);
            return (T)obj;
        }
    }
}

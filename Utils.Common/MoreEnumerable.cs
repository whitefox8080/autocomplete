﻿namespace Utils.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class MoreEnumerable
    {
        public static IEnumerable<T> Except<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                   equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where ReferenceEquals(null, temp) || temp.Equals(default(T))
                   select item;
        }

        public static IEnumerable<T> Intersect<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            return from otherItem in other
                   join item in items on getKey(otherItem)
                   equals getKey(item)
                   select otherItem;
        }
    }
}

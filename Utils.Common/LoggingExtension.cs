﻿
namespace Utils.Common
{
    using Microsoft.Extensions.Logging;
    using System;
    public static class LoggingExtension
    {
        public static void Info(this ILogger logger, Func<string> message)
        {
            if (logger == null) return;
            if (message == null) return;
            if (!logger.IsEnabled(LogLevel.Information)) return;

            logger.LogInformation(message());
        }
        public static void Trace(this ILogger logger, Func<string> message)
        {
            if (logger == null) return;
            if (message == null) return;
            if (!logger.IsEnabled(LogLevel.Trace)) return;

            logger.LogTrace(message());
        }

        public static void Debug(this ILogger logger, Func<string> message)
        {
            if (logger == null) return;
            if (message == null) return;
            if (!logger.IsEnabled(LogLevel.Debug)) return;

            logger.LogDebug(message());
        }

        public static void Warn(this ILogger logger, Func<string> message)
        {
            if (logger == null) return;
            if (message == null) return;
            if (!logger.IsEnabled(LogLevel.Warning)) return;

            logger.LogWarning(message());
        }

        public static void Error(this ILogger logger, Func<string> message)
        {
            if (logger == null) return;
            if (message == null) return;
            if (!logger.IsEnabled(LogLevel.Error)) return;

            logger.LogError(message());
        }

        public static void Critical(this ILogger logger, Func<string> message)
        {
            if (logger == null) return;
            if (message == null) return;
            if (!logger.IsEnabled(LogLevel.Critical)) return;

            logger.LogCritical(message());
        }
    }
}

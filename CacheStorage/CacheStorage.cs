﻿using System;
using AutoComplete.CacheStorage.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace AutoComplete.CacheStorage
{
    public class CacheStorage<K, V> : ICacheStorage<K, V>
    {
        private readonly MemoryCacheEntryOptions _defaultEntryOptions;
        private readonly MemoryCache _cache;

        public CacheStorage(TimeSpan timeout)
        {
            _defaultEntryOptions = new MemoryCacheEntryOptions
            {
                Priority = CacheItemPriority.NeverRemove,
                SlidingExpiration = timeout
            };

            var options = new MemoryCacheOptions { ExpirationScanFrequency = TimeSpan.FromSeconds(1) };
            _cache = new MemoryCache(options);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public V GetOrCreate(K key, Func<V> factory)
        {
            if (_cache.TryGetValue(key, out V item))
            {
                return item;
            }

            item = factory();
            _cache.Set(key, item, _defaultEntryOptions);

            return item;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public bool TryGet(K key, out V item)
        {
            var got = _cache.TryGetValue(key, out item);
            return got;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public void Remove(K key)
        {
            _cache.Remove(key);
        }
    }
}


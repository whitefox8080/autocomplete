# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Quick summary: Service for word usage

## AutoComplete.Cli. Simple usage.
 
1.   Create database: 	-c <file.txt>
2.   Update database: 	-u <file.txt>
3.   Drop database:	-d
4.   Enter a prefix of any word: 

```sh
ab
-- abort
-- any
-- ask
```

## Server-Service-Clients: 
###  Start a server: 
```sh
AutoComplete.Server -p 12345 -db=C:\Words.mdf
```
###  Start a client:
```sh
AutoComplete.Client -p 12345
```
###  Enter the following commands:
- Create database: -c <file.txt>
```sh
-c file1.txt
```
- Update database: -u <file.txt>
```sh
-u file2.txt
```
- Drop database: -d
```sh
-d
```
- Prefix: any word
```sh
ask
```
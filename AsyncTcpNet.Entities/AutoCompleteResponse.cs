﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoComplete.AsyncTcpNet.Entities
{
	[Serializable]
	public class Response
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="Response"/> class.
		/// </summary>
		public Response()
		{
		}

		/// <summary>
		/// gets or sets the list of frequency words
		/// </summary>
		public List<string> Words { get; set; }

		/// <summary>
		/// gets or sets a request message
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// gets or sets the operation result
		/// </summary>
		public OperationResult OperationResult { get; set; }
	}

	public enum CommandCode
	{
		/// <summary>
		/// Create
		/// </summary>
		Create,

		/// <summary>
		/// Update
		/// </summary>
		Update,

		/// <summary>
		/// Drop
		/// </summary>
		Drop,

		/// <summary>
		/// AutoComplete
		/// </summary>
		AutoComplete
	}

	public enum ResultCode
	{
		Success,
		Error
	}

	/// <summary>
	/// Result code
	/// </summary>
	[Serializable]
	public class OperationResult
	{
		/// <summary>
		/// Result code
		/// </summary>
		public ResultCode ResultCode { get; set; }

		/// <summary>
		/// Operation code
		/// </summary>
		public CommandCode CommandCode { get; set; }
	}
}

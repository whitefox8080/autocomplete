﻿namespace AutoCompleteService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AutoCompleteMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Words",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Value = c.String(maxLength: 15),
                        Qty = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Words");
        }
    }
}

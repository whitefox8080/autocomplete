﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoCompleteService.DAL.Model
{
    public class WordDto
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public int Qty { get; set; }

        public override string ToString() => $"Id: {Id}, Value: {Value}, Qty: {Qty}";
    }
}

﻿using System.Data.Entity;
using AutoCompleteService.DAL.Model;

namespace AutoCompleteService.DAL.Configuration
{
    public class AutoCompleteContext: DbContext
    {
        public AutoCompleteContext() : base("name=AutoCompleteContext")
        {
            //Database.SetInitializer<AutoCompleteContext>(new Initializer());
        }

        public virtual DbSet<WordDto> Words { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Configurations.Add(new ComparedValueConfiguration());
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }

    public class AutoCompleteContextDbAttached : AutoCompleteContext
    {
        public AutoCompleteContextDbAttached(string connectionString)
        {
            Database.Connection.ConnectionString = connectionString;
        }
    }
}

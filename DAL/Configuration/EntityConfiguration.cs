﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using AutoCompleteService.DAL.Model;

namespace AutoCompleteService.DAL.Configuration
{
    public class ComparedValueConfiguration : EntityTypeConfiguration<WordDto>
    {
        private readonly string _wordTable = "Words";

        public ComparedValueConfiguration()
        {
            ToTable(_wordTable);
            HasKey(p => p.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.Value).HasMaxLength(15);
            Property(p => p.Qty);
        }
    }
}

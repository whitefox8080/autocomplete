﻿using System.Collections.Generic;
using System.Data.Entity;
using AutoCompleteService.DAL.Model;

namespace AutoCompleteService.DAL.Configuration
{
    public class Initializer : //DropCreateDatabaseAlways<HoroscopeEntities>
        CreateDatabaseIfNotExists<AutoCompleteContext>
    {
        protected override void Seed(AutoCompleteContext ctx)
        {
            //CreateWordsContent(ctx);
        }

        private void CreateWordsContent(AutoCompleteContext ctx)
        {
            var entries = GenerateWordList();

            ctx.Words.AddRange(entries);
            ctx.SaveChanges();
        }

        private List<WordDto> GenerateWordList()
        {
            var result = new List<WordDto>
            {
                #region Initial test data
                new WordDto { Value = "бло", Qty = 5 },
                new WordDto { Value = "блок", Qty = 4 },
                new WordDto { Value = "блоха", Qty = 4 },
                new WordDto { Value = "блондинка", Qty = 3 },
                new WordDto { Value = "блокпост", Qty = 3 },
                new WordDto { Value = "блокада", Qty = 6 }

                #endregion
            };

            return result;
        }
    }
}
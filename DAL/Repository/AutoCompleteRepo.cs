﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace AutoCompleteService.DAL.Repository
{
    public class AutoCompleteRepo<TEntity> : IAutoCompleteRepo<TEntity>, IDisposable where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly DbContext _context;
        private bool _disposed = false;

        public AutoCompleteRepo(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public int Create(TEntity item)
        {
            _dbSet.Add(item);
            return SaveChanges();
        }

        public TEntity FindById(int id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> Get() => _dbSet.AsNoTracking().ToList();

        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate) => _dbSet.AsNoTracking().Where(predicate).ToList();

        public TEntity GetOne(Func<TEntity, bool> predicate) => Get(predicate).FirstOrDefault();

        public int Remove(TEntity item)
        {
            _dbSet.Attach(item);
            _dbSet.Remove(item);
            return SaveChanges();
        }

        public int Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            return SaveChanges();
        }

        internal int SaveChanges()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return -5001;
            }
            catch (DbUpdateException)
            {
                return -5002;
            }
            catch (CommitFailedException)
            {
                return -5003;
            }
            catch (Exception)
            {
                return -5000;
            }
        }

        #region Disposing

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing) _context.Dispose();
            _disposed = true;
        }

        #endregion
    }
}

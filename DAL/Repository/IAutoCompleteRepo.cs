﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AutoCompleteService.DAL.Repository
{
    public interface IAutoCompleteRepo<TEntity> where TEntity : class
    {
        int Create(TEntity item);
        TEntity FindById(int id);
        IEnumerable<TEntity> Get();
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);
        int Remove(TEntity item);
        int Update(TEntity item);
    }
}

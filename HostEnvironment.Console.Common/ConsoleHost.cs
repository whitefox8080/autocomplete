﻿
namespace HostEnvironment.Console.Common
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    /// <summary>
    /// ConsoleHost
    /// </summary>
    public abstract class ConsoleHost
    {
        public async Task Run(string[] args)
        {
            await Start(args);

            await WaitForShutdownAsync();
            await Stop(args);
            Environment.Exit(0);
        }

        public abstract Task Start(string[] args);

        public abstract Task Stop(string[] args);

        public static void SetConsoleHostTitle(string title)
        {
            Console.Title = title;
        }

        /// <summary>
        /// WaitForShutdown
        /// </summary>
        public void WaitForShutdown()
        {
            WaitForShutdownAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// Wait
        /// </summary>
        public void Wait()
        {
            WaitAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// WaitAsync
        /// </summary>
        public async Task WaitAsync(CancellationToken token = default)
        {
            if (token.CanBeCanceled)
            {
                await WaitAsync(token, shutdownMessage: null);
                return;
            }

            var done = new ManualResetEventSlim(false);
            using (var cts = new CancellationTokenSource())
            {
                AttachCtrlcSigtermShutdown(cts, done, shutdownMessage: "Application is shutting down...");
                await WaitAsync(cts.Token, "Application running. Press Ctrl+C to shut down.");
                done.Set();
            }
        }

        public async Task WaitForShutdownAsync(CancellationToken token = default)
        {
            var done = new ManualResetEventSlim(false);
            using (var cts = CancellationTokenSource.CreateLinkedTokenSource(token))
            {
                AttachCtrlcSigtermShutdown(cts, done, shutdownMessage: string.Empty);
                await WaitForTokenShutdownAsync(cts.Token);
                done.Set();
            }
        }

        protected async Task WaitAsync(CancellationToken token, string shutdownMessage)
        {
            if (!string.IsNullOrEmpty(shutdownMessage))
            {
                Console.WriteLine(shutdownMessage);
            }

            await WaitForTokenShutdownAsync(token);
        }

        protected void AttachCtrlcSigtermShutdown(CancellationTokenSource cts, ManualResetEventSlim resetEvent, string shutdownMessage)
        {
            void ShutDown()
            {
                if (!cts.IsCancellationRequested)
                {
                    if (!string.IsNullOrWhiteSpace(shutdownMessage))
                    {
                        Console.WriteLine(shutdownMessage);
                    }

                    try
                    {
                        cts.Cancel();
                    }
                    catch (ObjectDisposedException)
                    {
                    }
                }

                //Wait on the given reset event
                resetEvent.Wait();
            }

            AppDomain.CurrentDomain.ProcessExit += delegate { ShutDown(); };
            Console.CancelKeyPress += (sender, eventArgs) => {
                ShutDown();
                //Don't terminate the process immediately, wait for the Main thread to exit gracefully.
                eventArgs.Cancel = true;
            };
        }

        protected async Task WaitForTokenShutdownAsync(CancellationToken token)
        {
            var waitForStop = new TaskCompletionSource<object>();
            token.Register(obj => {
                var tcs = (TaskCompletionSource<object>)obj;
                tcs.TrySetResult(null);
            }, waitForStop);

            await waitForStop.Task;
        }
    }
}

